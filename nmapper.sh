#!/bin/bash

RED="\033[01;31m"
GREEN="\033[01;32m"
YELLOW="\033[01;33m"
RESET="\033[00m"

#-- check for root or exit
if [ $EUID != 0 ]
then
    echo -e "\n[${RED}!${RESET}] must be ${RED}root${RESET}"
    exit 1
fi

declare -a files=("./targets.ip" "./exclude.ip")
declare -a folders=("scans" "open-ports" "nse_scans")

for file in ${files[*]}
do
    if [ ! -f "$file" ]
    then
	touch $file
        echo -e "\n[${GREEN}+${RESET}] Populate the ${YELLOW} $file ${RESET} file"
	exit 1
    fi
done

for folder  in ${folders[*]}
do
    if [ ! -d "$folder" ]
    then
	mkdir -p $folder         
    fi
done

#- Nmap variables
MINHOST=$1
if  [[ -z "$MINHOST" ]]; then
    MINHOST=50
fi

MINRATE=$2
if  [[ -z "$MINRATE" ]]; then
    MINRATE=500
fi

#- port variables
PORTRANGE=$3
if  [[ -z "$PORTRANGE" ]]; then
    PORTRANGE=1-1024
fi
MINPORT=$(echo $PORTRANGE | cut -d '-' -f 1)
MAXPORT=$(echo $PORTRANGE | cut -d '-' -f 2)


#- nmap
pingSweep(){
    echo -e "\n[${GREEN}+${RESET}] Running ${YELLOW}nmap${RESET} scans"
    echo -e "\n[${GREEN}+${RESET}] Running an ${YELLOW}nmap ping sweep${RESET} for all ip in targets.ip"
    nmap --open -sn -PE -iL targets.ip \
	 -oA scans/PingSweep --excludefile exclude.ip --min-hostgroup $MINHOST --min-rate=$MINRATE
    grep "Up" scans/PingSweep.gnmap | cut -d " " -f2 | sort -u > ./alive.ip
}

nmapPortScan(){
    echo -e "\n[${GREEN}+${RESET}] Running an ${YELLOW}nmap port scan${RESET} for all ip in nmap/alive.ip"
    nmap --open -iL ./alive.ip \
	 -sU -sT -sV -O -Pn -n -oA scans/portscan -v \
	 -p T:$PORTRANGE,U:53,69,123,161,500,1434 \
	 --min-hostgroup $MINHOST --min-rate=$MINRATE
}


#- progress bar
#-- https://unix.stackexchange.com/questions/415421/linux-how-to-create-simple-progress-bar-in-bash
prog() {
    local w=80 p=$1;  shift
    # create a string of spaces, then change them to dots
    printf -v dots "%*s" "$(( $p*$w/$MAXPORT ))" ""; dots=${dots// /#};
    # print those dots on a fixed-width space plus the percentage etc. 
    printf "\r\e[K|%-*s| %3d  %s" "$w" "$dots" "$p" "$*"; 
}

#- parse pingsweep
parser(){
    echo -e "\n[${GREEN}+${RESET}] Running ${YELLOW}parser${RESET} for ${YELLOW}nse${RESET} scans"
 
    for n in $(seq $MINPORT $MAXPORT);   
    do
	if [ $(cat scans/*.gnmap | egrep " $n\/open\/tcp/" | cut -d " " -f 2 | wc -l) -eq '0' ];
	then
	    prog "$n" out of $MAXPORT TCP ports...
	else
	    cat scans/*.gnmap | egrep " $n\/open\/tcp/" | cut -d " " -f 2 >> open-ports/$n.txt
	fi
	if [ $(cat scans/*.gnmap | egrep " $n\/open\/udp/" | cut -d " " -f 2 | wc -l) -eq '0' ];
	then
	    prog "$n" out of $MAXPORT UDP ports...
	else
	    cat scans/*.gnmap | egrep " $n\/open\/udp/" | cut -d " " -f 2 >> open-ports/$n.txt
	fi
    done
}

nse(){
    echo -e "\n[${GREEN}+${RESET}] running ${YELLOW}nse${RESET} scans"
    if [ -f open-ports/21.txt ]; 
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}21${RESET}"
	nmap -sC -sV -p 21 -iL open-ports/21.txt \
	     --script=ftp-anon,ftp-bounce,ftp-proftpd-backdoor,ftp-vsftpd-backdoor -oN nse_scans/ftp.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/22.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}22${RESET}"
	nmap -sC -sV -p 22 -iL open-ports/22.txt \
	     --script=ssh2-enum-algos -oN nse_scans/ssh.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/23.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}23${RESET}"
	nmap -sC -sV -p 23 -iL open-ports/23.txt \
	     --script=telnet-encryption,banner,telnet-ntlm-info,tn3270-info -oN nse_scans/telnet.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/25.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}25${RESET}"
	nmap -sC -sV -p 25 -iL open-ports/25.txt \
	     --script=smtp-commands,smtp-open-relay,smtp-ntlm-info,smtp-enum-users.nse \
	     --script-args smtp-enum-users.methods={EXPN,VRFY} -oN nse_scans/smtp.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/53.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}53${RESET}"
	nmap -sU -p 53 -iL open-ports/53.txt \
	     --script=dns-recursion,dns-service-discovery,dns-cache-snoop.nse,dns-nsec-enum \
	     --script-args dns-nsec-enum.domains=example.com -oN nse_scans/dns.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/80.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}80${RESET}"
	nmap -sC -sV -p 80 -iL open-ports/80.txt \
	     --script=http-default-accounts,http-enum,http-title,http-methods,http-robots.txt,http-trace,http-shellshock,http-dombased-xss,http-phpself-xss,http-wordpress-enum,http-wordpress-users \
	     -oN nse_scans/http.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/110.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}110${RESET}"
	nmap -sC -sV -p 110 -iL open-ports/110.txt \
	     --script=pop3-capabilities -oN nse_scans/pop3.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/111.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}111${RESET}"
	nmap -sV -p 111 -iL open-ports/111.txt \
	     --script=nfs-showmount,nfs-ls -oN nse_scans/nfs111.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/123.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}123${RESET}"
	nmap -sU -p 123 -iL open-ports/123.txt \
	     --script=ntp-info,ntp-monlist -oN nse_scans/ntp.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/161.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}161${RESET}"
	nmap -sC -sU -p 161 -iL open-ports/161.txt \
	     --script=snmp-interfaces,snmp-sysdescr,snmp-netstat,snmp-processes -oN nse_scans/snmp.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/443.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}443${RESET}"
	nmap -sC -sV -p 443 -iL open-ports/443.txt \
	     --script=http-default-accounts,http-title,http-methods,http-robots.txt,http-trace,http-shellshock,http-dombased-xss,http-phpself-xss,http-wordpress-enum \
	     -oN nse_scans/https.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE

	nmap -sC -sV -p 443 -iL open-ports/443.txt --version-light \
	 --script=ssl-poodle,ssl-heartbleed,ssl-enum-ciphers,ssl-cert-intaddr \
	 --script-args vulns.showall -oN nse_scans/ssl.txt \
	 --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/445.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}445${RESET}"
	nmap -sC -sV  -p 445 -iL open-ports/445.txt \
	     --script=smb-enum-shares.nse,smb-os-discovery.nse,smb-enum-users.nse,smb-security-mode,smb-vuln-ms17-010,smb-vuln-ms08-067,smb2-vuln-uptime \
	     -oN nse_scans/smb.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/1521.txt ];
    then
	nmap -p 1521-1560 -iL open-ports/1521.txt \
	     --script=oracle-sid-brute -oN nse_scans/oracle.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/2049.txt ];
    then
	nmap -sV -p 2049 -iL open-ports/2049.txt \
	     --script=nfs-showmount,nfs-ls -oN nse_scans/nfs2049.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/3306.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}3306${RESET}"
	nmap -sC -sV -p 3306 -iL open-ports/3306.txt \
	     --script=mysql-empty-password,mysql-users,mysql-enum,mysql-audit \
	     --script-args "mysql-audit.username='root', \mysql-audit.password='foobar',mysql-audit.filename='nselib/data/mysql-cis.audit'" \
	     -oN nse_scans/mysql.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    
	nmap -sC -sV -p 3306 -iL open-ports/3306.txt \
	     --script=mysql-empty-password,mysql-users,mysql-enum,mysql-audit \
	     --script-args "mysql-audit.username='root', \mysql-audit.password='foobar',mysql-audit.filename='nselib/data/mysql-cis.audit'" \
	     -oN nse_scans/mysql.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/5900.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}5900${RESET}"
	nmap -sC -sV -p 5900 -iL open-ports/5900.txt \
	     --script=banner,vnc-title -oN nse_scans/vnc.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/8080.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}8080${RESET}"
	nmap -sC -sV -p 8080 -iL open-ports/8080.txt \
	     --script=http-default-accounts,http-title,http-robots.txt,http-methods,http-shellshock,http-dombased-xss,http-phpself-xss \
	     -oN nse_scans/http8080.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/8443.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}8443${RESET}"
	nmap -sC -sV -p 8443 -iL open-ports/8443.txt \
	     --script=http-default-accounts,http-title,http-robots.txt,http-methods,http-shellshock,http-dombased-xss,http-phpself-xss \
	     -oN nse_scans/https8443.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
    if [ -f open-ports/27017.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}27017${RESET}"
	nmap -sC -sV -p 27017 -iL open-ports/27017.txt \
	     --script=mongodb-info,mongodb-databases -oN nse_scans/mongodb.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
    else
	:
    fi
}

#- other scans
otherScans(){
    echo -e "\n[${GREEN}+${RESET}] running ${YELLOW}other${RESET} scans"
    if [ -f open-ports/500.txt ];
    then
	echo -e "\n[${GREEN}+${RESET}] running scans for port ${YELLOW}500${RESET}"
	nmap -sU -p 500 -iL open-ports/500.txt \
	     --script=ike-version -oN nse_scans/ike.txt \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
	for ip in $(cat open-ports/500.txt)
	do	    
	    ike-scan -A -M $ip --id=GroupVPN | tee -a nse_scans/IKE-$ip.txt
	done
    else
	:
    fi
}

htmlReport(){
    echo -e "\n[${GREEN}+${RESET}] generating ${YELLOW}html${RESET} outputs"
    cd scans/
    for file in $(ls ./*.xml)
    do
	xsltproc $file -o $file.html
    done
}

#- summary
summary(){
    echo -e "\n[${GREEN}+${RESET}] Generating a summary of the scans..."
    for ip in $(cat ./alive.ip); do
	echo -e $ip > ./open-ports/$ip.txt
	awk \/$ip\/ scans/portscan.gnmap | egrep -o '*[0-9]*/open/*[tcp/udp]*/' | sort | uniq | awk -F '/' '{print $1"/"$3}' >> ./open-ports/$ip.txt
    done
    echo -e "\n[${GREEN}+${RESET}] there are $( wc -l ./alive.ip ) ${YELLOW}alive hosts${RESET} and $(egrep -o '[0-9]*/open/' scans/*.gnmap | cut -d ':' -f 2 | sort | uniq | wc -l) ${YELLOW}unique ports/services${RESET}" | \
	tee -a discovered-ports.txt
}

#- run them
pingSweep
nmapPortScan
parser
nse
otherScans
htmlReport
summary
